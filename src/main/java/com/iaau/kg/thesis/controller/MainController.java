package com.iaau.kg.thesis.controller;

import com.iaau.kg.thesis.dao.TweetDao;
import com.iaau.kg.thesis.model.Followers;
import com.iaau.kg.thesis.model.Tweet;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainController extends Thread {

    private final String POST_TWEET_URL = "https://twitter.com/i/tweet/create";
    private final String POST_DM_URL = "https://twitter.com/i/direct_messages/new";
    Response likeTweetResponse;
    private String email;
    private String password;
    private String userAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:48.0) Gecko/20100101 Firefox/48.0";
    private String myUserId;
    private String myUserName;
    private String authenticity_token;
    private String status;
    private String likeMessage;
    private Map<String, String> cookies;
    private List<Tweet> tweetList;
    private TweetDao tweetDao;
    private boolean sendDm = false;
    private String directMessage;
    private boolean tweetOrNot = false;
    private String tweetMessage = "empty";
    private Response directMessageResponse;

    public MainController(String email, String password) {
        this.email = email;
        this.password = password;
        tweetDao = new TweetDao();
        status = "RUNNING";

    }

    public void run() {
        System.out.println(" thread is running ");
        boolean ok = true;
        boolean switcher = true;
        int co = 0;

        while (ok) {
            System.out.println(co + " " + currentThread());
            co++;
            try {
                //TODO call getTweets function
                getTweets();
                System.out.println("get tweets function called");
                switcher = !switcher;
                Thread.sleep(5000);  // current thread sleeps 5 seconds
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (switcher) {
                //TODO call like tweets function
                try {
                    likeTweet(likeMessage); // message text to be liked
                    System.out.println("like tweet function called");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (sendDm)  // if direct message is ready send it and make it false
            {
                try {
                    sendDM(directMessage, directMessageResponse);
                } catch (IOException e) {
                    System.out.println("Unable to send DM");
                    //e.printStackTrace();
                }
                sendDm = false;
            }

            if (tweetOrNot)  // Tweet
            {
                try {
                    createTweet(tweetMessage);
                } catch (IOException e) {
                    System.out.println("Unable to Tweet: " + tweetMessage);
                    //e.printStackTrace();
                }
                tweetOrNot = false;
            }


            //Terminate program if requirements match
            if (status.equals("STOP")) {
                ok = false;
            }

        }

        System.out.println(getMyUserName() + " stopped acting");
    }

    public Map<String, String> login() {

        Map<String, String> inputs = new HashMap<>();
        Response response;
        try {
            response = Jsoup.connect("https://twitter.com/login")
                    .userAgent(userAgent).execute();

            authenticity_token = response.parse().select("input[name=authenticity_token]").first().attr("value");
            inputs.put("session[username_or_email]", email);
            inputs.put("session[password]", password);
            inputs.put("authenticity_token", authenticity_token);
            inputs.put("remember_me", "1");
            System.out.println(authenticity_token + " " + password + " " + email);
            Response loginResponse = Jsoup
                    .connect("https://twitter.com/sessions")
                    .userAgent(userAgent).data(inputs).method(Method.POST)
                    .cookies(response.cookies()).execute();
            cookies = loginResponse.cookies();

            directMessageResponse = loginResponse;
            myUserName = loginResponse.parse().getElementsByAttributeValue("data-nav", "view_profile").attr("href");
            myUserId = loginResponse.parse().getElementsByAttributeValue("data-screen-name", myUserName.replace("/", "")).attr("data-user-id");

        } catch (IOException e) {
            e.printStackTrace();
        }

        return cookies;
    }

    public void createTweet(String message) throws IOException {
        Map<String, String> map = new HashMap<>();
        map.put("authenticity_token", authenticity_token);
        map.put("is_permalink_page", "false");
        map.put("status", message);
        map.put("place_id", "");
        map.put("tagged_user", "");

        Response response = Jsoup.connect(POST_TWEET_URL)
                .cookies(cookies)
                .userAgent(userAgent)
                .data(map)
                .timeout(100000)
                .referrer("https://twitter.com")
                .header("Connection", "keep-alive")
                .header("Content-Type", "text/*")
                .header("X-Requested-With", "XMLHttpRequest")
                .header("Accept", "application/json, text/javascript, */*; q=0.01")
                .method(Method.POST)
                .execute();

    }

    public List<Tweet> getTweets() throws IOException {

        tweetList = new ArrayList<>();
        List<Tweet> tweets = new ArrayList<>();
        likeTweetResponse = Jsoup.connect("https://twitter.com")
                .userAgent(userAgent)
                .cookies(cookies)
                .execute();
        Element streamClass = likeTweetResponse.parse().getElementById("stream-items-id");
        String id;
        Elements li = streamClass.getElementsByAttributeValue("data-item-type", "tweet");
        for (Element elementOfLiTag : li) {
            Elements span = elementOfLiTag.select("span.ProfileTweet-actionCount");
            id = elementOfLiTag.attr("data-item-id");
            String text = elementOfLiTag.getElementsByClass("TweetTextSize").text();
            Tweet tweet = new Tweet(text, id, false);
            tweets.add(tweet);
//            System.out.println(tweet.toString());
        }
        tweetDao.saveTweet(tweets, myUserName.replace("/", "") + "_tweet");
        return tweetList;
    }


    public void likeTweet(String keyword) throws IOException {
        List<Tweet> filteredTweetByKeyword =
                tweetDao.getTweetByKeyword(keyword, myUserName.replace("/", "") + "_tweet");
        Map<String, String> inputs = new HashMap<>();

        for (int i = 0; i < filteredTweetByKeyword.size(); i++) {
            inputs.put("authenticity_token", authenticity_token);
            inputs.put("id", filteredTweetByKeyword.get(i).getId());
            inputs.put("tweet_stat_count", "");
            try {
                Jsoup.connect("https://twitter.com/i/tweet/like")
                        .userAgent(userAgent)
                        .data(inputs)
                        .cookies(cookies)
                        .referrer("https://twitter.com")
                        .header("Connection", "keep-alive")
                        .header("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
                        .header("Content-Length", "100")
                        .header("X-Requested-With", "XMLHttpRequest")
                        .header("Accept-Encoding", "gzip, deflate, br")
                        .header("Accept", "application/json, text/javascript, */*; q=0.01")
                        .post();
                tweetDao.updateTweetIsLikeProperty(Long.parseLong(filteredTweetByKeyword.get(i).getId()), myUserName.replace("/", "") + "_tweet");
                System.out.println("liked");
            } catch (HttpStatusException e) {
                System.out.println("some problem but maybe already liked");
            } catch (Exception e) {
                System.out.println("could not post like tweet method " + e.getMessage());
            }
        }
    }

    public void sendDM(String message, Response loginResponse) throws IOException {
        Map<String, String> map = new HashMap<>();
        List<Followers> myFollowers = getTwitterFollowers(loginResponse.cookies(), loginResponse);
        for (int i = 0; i < myFollowers.size(); i++) {
            String referer = "https://twitter.com/" + myFollowers.get(i).getUsername();
            map.clear();
            map.put("authenticity_token", authenticity_token);
            map.put("conversation_id", myUserId + "-" + myFollowers.get(i).getDataUserId());
            map.put("scribeContext[component]", "tweet_box_dm");
            map.put("tagged_users", "");
            map.put("text", message);
            map.put("tweetboxId", String.valueOf(System.currentTimeMillis()));

            Response response = Jsoup.connect(POST_DM_URL)
                    .cookies(cookies)
                    .userAgent(userAgent)
                    .data(map)
                    .timeout(100000)
                    .referrer(referer)
                    .header("Connection", "keep-alive")
                    .header("Content-Type", "text/*")
                    .header("X-Requested-With", "XMLHttpRequest")
                    .header("Accept", "application/json, text/javascript, */*; q=0.01")
                    .method(Method.POST)
                    .execute();
            //   System.out.println(response.statusCode() + "##" + response.body());
        }
    }

    public List<Followers> getTwitterFollowers(Map<String, String> loginCookies,
                                               Response loginResponse) throws IOException {

        //make it dynamic mickey  loginResponse may be too old
        Response response = Jsoup.connect("https://twitter.com").userAgent(userAgent).cookies(cookies).execute();
        String urlToGetFollowers;

        myUserName = loginResponse.parse().getElementsByAttributeValue("data-nav", "view_profile").attr("href");
        myUserId = loginResponse.parse().getElementsByAttributeValue("data-screen-name", myUserName.replace("/", "")).attr("data-user-id");
        urlToGetFollowers = myUserName + "/followers";
        Document followersDocument = Jsoup
                .connect("https://twitter.com" + urlToGetFollowers)
                .cookies(loginCookies).userAgent(userAgent)
                .get();
        String role = followersDocument.attr("role");
        Elements followers = followersDocument.getElementsByAttributeValue("role", "listitem");
        List<Followers> myFollowers = new ArrayList<>();
        for (Element follow : followers) {
            Elements select = follow.select("div.ProfileCard");
            String dataScreenName = select.attr("data-screen-name");
            String dataUserId = select.attr("data-user-id");
            myFollowers.add(new Followers(dataScreenName, dataUserId));
        }
        return myFollowers;
    }


    public String getEmail() {
        return email;
    }

    public String getMyUserId() {
        return myUserId;
    }

    public String getMyUserName() {
        return myUserName;
    }

    public void setStatus(String status) {
        this.status = status;
        System.out.println(getEmail() + " twitter bot status changed to: " + status);
    }

    public void setLikeMessage(String likeMessage) {
        this.likeMessage = likeMessage;
    }

    public void setSendMessage(String pattern, boolean send) {
        this.sendDm = true;
        directMessage = pattern;
    }

    public void setTweetMessage(String pattern, boolean b) {
        tweetMessage = pattern;
        tweetOrNot = b;
    }
//    public void logOut(Map<String, String> cookies) throws IOException {
//        Response response = Jsoup.connect("https://twitter.com/logout").data("authenticity_token", authenticity_token).data("reliability_event", "").data("scribe_log", "").cookies(cookies).method(Method.POST).execute();
//        PrintWriter pr = new PrintWriter(new File("MYKY" + ".html"));
//        pr.print(response.body());
//        pr.close();
//    }
}
