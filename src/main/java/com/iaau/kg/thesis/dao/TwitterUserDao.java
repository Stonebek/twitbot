package com.iaau.kg.thesis.dao;

import com.iaau.kg.thesis.model.TwitterUser;

import java.sql.*;

/**
 * Created by Mickey on 9/15/16.
 */
public class TwitterUserDao {
    public void createTwitterUserDao() {
        Connection connection = MySQLConnection.connect();
        String sql = "CREATE TABLE twitter_users (" +
                "id bigint(20) NOT NULL," +
                "email varchar(100) NOT NULL," +
                "password varchar(100) NOT NULL," +
                "display_name varchar(255) NOT NULL," +
                "PRIMARY KEY (id)" +
                ") ENGINE=InnoDB DEFAULT CHARSET=utf8";
        try {
            DatabaseMetaData md = connection.getMetaData();
            ResultSet rs = md.getTables(null, null, "twitter_users", null);
            if (!rs.next()){
                System.out.println(sql);
                Statement statement = connection.createStatement();
                statement.executeUpdate(sql);
            } else {
                System.out.println("twitter_users table created already. good ");
            }
        } catch (SQLException e) {
            System.out.println("Table twitter_users problem");
//            e.printStackTrace();
        }
    }

    public int addTwitterUser(TwitterUser twitterUser) {
        Connection connection = MySQLConnection.connect();
        Long id = Long.parseLong(twitterUser.getId());
        String searchQuery = "SELECT id from twitter_users WHERE id = " + id;
        try {
            Statement statementForSearch = connection.createStatement();
            ResultSet resultSet = statementForSearch.executeQuery(searchQuery);
            if (!resultSet.next()) {
                Statement statement = connection.createStatement();
                String email = twitterUser.getEmail();
                String password = twitterUser.getPassword();
                String displayName = twitterUser.getDisplayName();
                int i = statement.executeUpdate("INSERT INTO twitter_users VALUES (" + id + "," + "'" + email + "'" + "," + "'" + password + "'" + "," + "'" + displayName + "'" + ")");
                return i;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void deleteTwitterUserById(Long id, String twitter_user_name) {
        Connection connection = MySQLConnection.connect();
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate("DELETE FROM twitter_users WHERE id=" + id);
            statement.executeUpdate("DROP TABLE " + twitter_user_name.replace("/", "") + "_tweet");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
