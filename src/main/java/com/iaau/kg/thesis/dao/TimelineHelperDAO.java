package com.iaau.kg.thesis.dao;

import java.sql.*;

/**
 * Created by Mickey on 9/16/16.
 */
public class TimelineHelperDAO {
    public void createTable(String tableName) {
        Connection connection = MySQLConnection.connect();
        String sql = "CREATE TABLE " + tableName + "_tweet" +
                "(" +
                "id BIGINT not NULL, " +
                " is_liked BOOLEAN, " +
                " twitter_text TEXT, " +
                " PRIMARY KEY ( id ) " +
                ") ENGINE=InnoDB DEFAULT CHARSET=utf8";
        try {
            DatabaseMetaData md = connection.getMetaData();
            ResultSet rs = md.getTables(null, null, tableName + "_tweet", null);
            boolean ok = true;
            while (rs.next()) {
                System.out.println(rs.getString(3));
                if (rs.getString(3).equals(tableName + "_tweet")) ok=false;
            }
            if (ok) {
                System.out.println(sql);
                Statement statement = connection.createStatement();
                statement.executeUpdate(sql);
            }
        } catch (SQLException e) {
            System.out.println("Table " + tableName + " already exists");
//            e.printStackTrace();
        }

    }
}
