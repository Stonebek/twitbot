package com.iaau.kg.thesis.dao;

import com.iaau.kg.thesis.model.Tweet;

import java.sql.*;
import java.util.*;

/**
 * Created by Mickey on 9/15/16.
 */
public class TweetDao {
    public void saveTweet(List<Tweet> tweets, String tableName) {
        Connection connection = MySQLConnection.connect();
        for (Tweet tweet : tweets) {
            Long id = Long.parseLong(tweet.getId());
            String searchQuery = "SELECT id from " + tableName + " WHERE id = " + id;
            try {
                Statement statementForSearch = connection.createStatement();
                ResultSet resultSet = statementForSearch.executeQuery(searchQuery);
                if (!resultSet.next()) {
                    String content = tweet.getText();
                    boolean isLike = tweet.getIsLiked();
                    String query = "INSERT INTO " + tableName + " (id, is_liked, twitter_text) values (?, ?, ?)";
                    PreparedStatement statement = connection.prepareStatement(query);
                    statement.setLong(1, id);
                    statement.setBoolean(2, isLike);
                    statement.setString(3, content);
                    statement.execute();
//                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public List<Tweet> getTweetByKeyword(String keyword, String tableName) {
//        String[] splitKeyword = keyword.split(" ");
        Connection connection = MySQLConnection.connect();
        List<Tweet> filteredTweetList = new ArrayList<>();
        String searchQuery = "SELECT * FROM " + tableName + " WHERE twitter_text like '%" + keyword + "%'";
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(searchQuery);
            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                Boolean isLiked = resultSet.getBoolean("is_liked");
                String twitterText = resultSet.getString("twitter_text");
                if(!isLiked)
                    filteredTweetList.add(new Tweet(twitterText, id.toString(), isLiked));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return filteredTweetList;
    }
    public void updateTweetIsLikeProperty(Long tweet_id,String tableName){
        String query = "UPDATE "+ tableName +
                " SET is_liked = 1 WHERE id="+tweet_id;
        Connection connection = MySQLConnection.connect();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            statement.executeUpdate(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}

