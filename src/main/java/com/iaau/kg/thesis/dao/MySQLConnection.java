package com.iaau.kg.thesis.dao;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Created by Mickey on 9/15/16.
 */
public class MySQLConnection {
    public static Connection connection;

    public static Connection connect() {
        if(connection!=null){
            return connection;
        }
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost/uk_gov_tariffs" + "?useUnicode=true&characterEncoding=UTF-8", "root", "");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return connection;
    }
}
