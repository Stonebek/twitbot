package com.iaau.kg.thesis.model;

/**
 * Created by Mickey on 9/7/16.
 */
public class Followers {
    private String username;
    private String dataUserId;

    public Followers(String username, String dataUserId) {
        this.username = username;
        this.dataUserId = dataUserId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDataUserId() {
        return dataUserId;
    }

    public void setDataUserId(String dataUserId) {
        this.dataUserId = dataUserId;
    }

    @Override
    public String toString() {
        return "Followers{" +
                "username='" + username + '\'' +
                ", dataUserId='" + dataUserId + '\'' +
                '}';
    }
}
