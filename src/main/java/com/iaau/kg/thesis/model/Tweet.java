package com.iaau.kg.thesis.model;

/**
 * Created by Mickey on 9/8/16.
 */
public class Tweet {
    private String text;
    private String id;
    private Boolean isLiked = false;

    public Tweet(String text, String id, Boolean isLiked) {
        this.text = text;
        this.id = id;
        this.isLiked = isLiked;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean getIsLiked() {
        return isLiked;
    }

    public void setIsLiked(Boolean isLiked) {
        this.isLiked = isLiked;
    }

    @Override
    public String toString() {
        return "Tweet{" +
                "text='" + text + '\'' +
                ", id='" + id + '\'' +
                ", isLiked=" + isLiked +
                '}';
    }
}
