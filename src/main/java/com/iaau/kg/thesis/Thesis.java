package com.iaau.kg.thesis;

import com.iaau.kg.thesis.controller.MainController;
import com.iaau.kg.thesis.dao.TwitterUserDao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Thesis implements InterestingEvent {
    private List<MainController> TwitterBots = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        //Create Twitter User Table
        TwitterUserDao twitterUserDao = new TwitterUserDao();
        twitterUserDao.createTwitterUserDao();

        InterestingEvent event = new Thesis();
        UserInterfaceWindow windows = new UserInterfaceWindow(event);

        // Test TWITTER ACCOUNTS
        String email = "elf49362@zasod.com";
        String password = "qwertyuiop1234567890";

        //   bsj12920@zasod.com
        //   capitanTimati

//        try {
//            MySQLConnection.connect().close();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
    }
    @Override
    public void startNewUser(String email, String pass) {
        System.out.println("Hello I received your message: " + email + " ### " + pass);
        MainController newBot = new MainController(email, pass);
        TwitterBots.add(newBot);
        newBot.login();
        newBot.start();
    }

    @Override
    public void stopUser(String s, String displayName, String email, String id) {
        MainController tmp;
        for (int i = 0; i < TwitterBots.size(); i++) {
            tmp = TwitterBots.get(i);
            if (tmp.getEmail().equals(email)) {
                tmp.setStatus("STOP");
                TwitterBots.remove(i);
                break;
            }
        }
    }

    @Override
    public void sendCMD(String email, String pattern, String cmd) {
        MainController tmp;
        for (int i = 0; i < TwitterBots.size(); i++) {
            tmp = TwitterBots.get(i);
            if (tmp.getEmail().equals(email)) {
                if (cmd.equals("LIKE")) {
                    tmp.setLikeMessage(pattern);
                    break;
                } else if (cmd.equals("SEND")) {
                    tmp.setSendMessage(pattern, true);
                    break;
                } else if (cmd.equals("TWEET")) {
                    tmp.setTweetMessage(pattern, true);
                    break;
                }
            }
        }
    }
}
