package com.iaau.kg.thesis;

/**
 * Created by omurbek on 9/4/2016.
 */
public interface InterestingEvent {

    void startNewUser(String email, String pass);

    void stopUser(String s, String displayName, String email, String id);

    void sendCMD(String email, String pattern, String cmd);
}
