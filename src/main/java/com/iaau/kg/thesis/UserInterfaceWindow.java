package com.iaau.kg.thesis;

import com.iaau.kg.thesis.dao.MySQLConnection;
import com.iaau.kg.thesis.dao.TimelineHelperDAO;
import com.iaau.kg.thesis.dao.TwitterUserDao;
import com.iaau.kg.thesis.model.TwitterUser;
import org.jsoup.Jsoup;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by omurbek on 9/17/2016.
 */
public class UserInterfaceWindow {

    public ImageIcon peopleIcon;
    public ImageIcon userIcon;
    public JTabbedPane tabbedPane = new JTabbedPane();
    public Dimension prefferedDimension = new Dimension(700, 500);
    JTable table;
    JFrame frame;
    DefaultTableModel dtm;

    List<String[]> data;
    TwitterUserDao twitterUserDao;
    InterestingEvent event;
    private JPanel mainPanel;
    private String userAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:48.0) Gecko/20100101 Firefox/48.0";

    public UserInterfaceWindow(InterestingEvent event) {
        twitterUserDao = new TwitterUserDao();
        this.event = event;
        initializeUserListPanel(); // initialize UserList tab
        userIcon = createImageIcon("images\\robot.png");
        mainPanel = new JPanel(new GridLayout(1, 1, 5, 5));
//        tabbedPane.setPreferredSize(new Dimension(500,100));
        tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
        mainPanel.add(tabbedPane);
        frame = new JFrame("Twitter BOT APP");
        frame.setSize(prefferedDimension);
        //  frame.setPreferredSize(prefferedDimension);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(mainPanel, BorderLayout.CENTER);
//        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    protected static ImageIcon createImageIcon(String path) {
        return new ImageIcon(path);
    }

    public JPanel userInAction(String email, String pass, String id) {

        JPanel newPanel = new JPanel(new GridLayout(4, 4, 5, 5));
        final JLabel email_l = new JLabel(email);
        JLabel pass_l = new JLabel(pass);
        JLabel id_l = new JLabel(id);
        final JLabel k_label = new JLabel("Search & Like");
        JLabel s_label = new JLabel("Send Direct Message");
        final JLabel t_label = new JLabel("Tweet");

        final JTextField k_field = new JTextField("Keywords here");
        final JTextField s_field = new JTextField("DM here");
        final JTextField t_field = new JTextField("Tweet message");

        //TODO buttons should listen actions
        JButton k_button = new JButton("Like");
        JButton s_button = new JButton("Send DM");
        JButton t_button = new JButton("Tweet");

        k_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String pattern = k_field.getText().trim();
                String email = email_l.getText().trim();
                String cmd = "LIKE";
                event.sendCMD(email, pattern, cmd);
            }
        });

        s_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String pattern = s_field.getText().trim();
                String email = email_l.getText().trim();
                String cmd = "SEND";
                event.sendCMD(email, pattern, cmd);
            }
        });

        t_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String pattern = t_field.getText().trim();
                String email = email_l.getText().trim();
                String cmd = "TWEET";
                event.sendCMD(email, pattern, cmd);
            }
        });


        //add user details
        newPanel.add(id_l);
        newPanel.add(email_l);
        newPanel.add(pass_l);

        newPanel.add(k_label);
        newPanel.add(k_field);
        newPanel.add(k_button);
        // text textfield button

        newPanel.add(s_label);
        newPanel.add(s_field);
        newPanel.add(s_button);

        newPanel.add(t_label);
        newPanel.add(t_field);
        newPanel.add(t_button);
        return newPanel;
    }

    public void addNewUserToTab(String displayName, String email, String pass, String id) {
        JPanel newPanel = userInAction(email, pass, id);
        tabbedPane.addTab(displayName, newPanel);
    }

    public List<String[]> getUserListData() {
        Connection connection = MySQLConnection.connect();
        List<String[]> data = new ArrayList<>();
        String searchQuery = "SELECT * FROM twitter_users";
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(searchQuery);
            while (resultSet.next()) {
                Long id = (resultSet.getLong("id"));
                String email = resultSet.getString("email");
                String displayName = resultSet.getString("display_name");
                String pass = resultSet.getString("password");
                String[] res = {String.valueOf(id), displayName, email, pass, "Run", "Stop", "Delete User"};
                data.add(res);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }


    public void initializeUserListPanel() {
        peopleIcon = createImageIcon("images\\people.png");  // create icon
        JPanel userPanel = new JPanel();    // create new jpanel to hold user info

        String[] columnNames =
                {"UserId",
                        "DisplayName",
                        "Email", "Password", "Run", "Stop", "Delete User"
                };
        data = getUserListData();
        int size = data.size();

        dtm = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false; // or a condition at your choice with row and column
            }
        };

        dtm.setColumnIdentifiers(columnNames);

        for (int i = 0; i < size; i++) {
            dtm.addRow(data.get(i));
        }


        table = new JTable();
        table.setModel(dtm);
        table.getTableHeader().setReorderingAllowed(false);
        TableCellRenderer buttonRenderer = new JTableButtonRenderer();
        table.getColumn("Run").setCellRenderer(buttonRenderer);
        table.getColumn("Stop").setCellRenderer(buttonRenderer);
        table.getColumn("Delete User").setCellRenderer(buttonRenderer);
        table.setPreferredScrollableViewportSize(new Dimension(500, 100));
        table.setFillsViewportHeight(true);
        JScrollPane scrollPane = new JScrollPane(table);


        table.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                JTable target = (JTable) e.getSource();
                int row = target.getSelectedRow();
                int column = target.getSelectedColumn();
                // do some action if appropriate column
                String userId;
                if (row != -1 && column != -1) {
                    String id = target.getValueAt(row, 0).toString();
                    String displayName = target.getValueAt(row, 1).toString();
                    String email = target.getValueAt(row, 2).toString();
                    String pass = target.getValueAt(row, 3).toString();

                    System.out.println(" Table Column name: " +
                            target.getModel().getColumnName(column));
                    String headerName = target.getModel().getColumnName(column);
                    if (headerName.equals("Run")) {
                        //TODO Create NEW TAB and Run new THREAD

                        addNewUserToTab(displayName, email, pass, id);
                        event.startNewUser(email, pass);


                    } else if (headerName.equals("Stop")) {
                        //TODO STOP THREAD & Remove new Tab
                        event.stopUser(id, displayName, email, pass);
                        deleteUserFromTab(displayName);
                        System.out.println("stop cmd executed ");

                    } else if (headerName.equals("Delete User")) {
                        //TODO STOP THREAD
                        //TODO DELETE From Database;
                        event.stopUser(id, displayName, email, pass);
                        deleteUserFromTab(displayName);
                        //Delete from Database
                        twitterUserDao.deleteTwitterUserById(Long.valueOf(id), displayName);
                        dtm.removeRow(row);
                    }

                    System.out.println(row + " % " + column + " clicked");
                }
            }
        });

        userPanel.add(scrollPane);                // user list view
        userPanel.add(loginPanel());              // Add new login panel
        tabbedPane.addTab("tab", peopleIcon, userPanel, "List of Twitter users");
    }

    private void deleteUserFromTab(String displayName) {
        for (int i = 0; i < tabbedPane.getTabCount(); i++) {
            String tabTitle = tabbedPane.getTitleAt(i);
            if (tabTitle.equals(displayName)) {
                tabbedPane.remove(i);
                break;
            }
        }
    }


    // Generate LOGIN PANEL and RETURN IT
    public JPanel loginPanel() {
        JPanel panel = new JPanel(new GridLayout(3, 2, 5, 5));
        panel.setSize(new Dimension(100, 100));
        JLabel l_name = new JLabel("Email");
        JLabel l_pass = new JLabel("Password");
        final JTextField t_name = new JTextField(10);
        final JPasswordField t_pass = new JPasswordField(10);
        JButton button = new JButton("Add New User");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String email = t_name.getText().trim();
                String pass = t_pass.getText().trim();

                System.out.println(email + " # " + pass);
                String[] ans = login(email, pass);
                if (ans == null || ans.length < 2) {
                    JOptionPane.showMessageDialog(null, "Wrong username/Password",
                            "Not authorized", JOptionPane.ERROR_MESSAGE);
                } else {  //Correct Username & Password
                    TimelineHelperDAO timelineHelperDAO = new TimelineHelperDAO();
                    timelineHelperDAO.createTable(ans[0].replace("/", ""));

                    System.out.println("table created" + ans[0] + " $ " + ans[1]);
                    twitterUserDao.addTwitterUser(new TwitterUser(ans[1], email, pass, ans[0])
                    );

                    String[] res = {ans[1], ans[0], email, pass, "Run", "Stop", "Delete User"};
                    dtm.addRow(res);
                    System.out.println("new row added to table model");
                }
            }
        });
        panel.add(l_name);
        panel.add(t_name);
        panel.add(l_pass);
        panel.add(t_pass);
        panel.add(button);
        return panel;
    }


    public String[] login(String email, String password) {

        Map<String, String> inputs = new HashMap<>();
        org.jsoup.Connection.Response response;
        try {
            response = Jsoup.connect("https://twitter.com/login")
                    .userAgent(userAgent).execute();

            String authenticity_token = response.parse()
                    .select("input[name=authenticity_token]")
                    .first().attr("value");
            inputs.put("session[username_or_email]", email);
            inputs.put("session[password]", password);
            inputs.put("authenticity_token", authenticity_token);
            inputs.put("remember_me", "1");
            System.out.println(authenticity_token + " " + password + " " + email);
            org.jsoup.Connection.Response loginResponse = Jsoup
                    .connect("https://twitter.com/sessions")
                    .userAgent(userAgent).data(inputs).method(org.jsoup.Connection.Method.POST)
                    .cookies(response.cookies()).execute();
            String myUserName = loginResponse.parse().getElementsByAttributeValue("data-nav", "view_profile").attr("href");
            if (!myUserName.isEmpty() && myUserName != null) {
                String myUserId = loginResponse.parse().getElementsByAttributeValue
                        ("data-screen-name", myUserName.replace("/", "")).attr("data-user-id");
                String[] ans = {myUserName, myUserId};
                return ans;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static class JTableButtonRenderer implements TableCellRenderer {
        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            JButton button = new JButton(value.toString());
            return button;
        }
    }
}
